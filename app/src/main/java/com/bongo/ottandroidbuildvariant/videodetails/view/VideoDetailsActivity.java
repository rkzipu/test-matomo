package com.bongo.ottandroidbuildvariant.videodetails.view;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bongo.ottandroidbuildvariant.R;
import com.bongo.ottandroidbuildvariant.analytics.AnalyticsController;
import com.bongo.ottandroidbuildvariant.utils.NetworkStateReceiver;
import com.bongo.ottandroidbuildvariant.videodetails.model.Content;
import com.bongobd.bongoplayerlib.BongoPlayer;
import com.bongobd.bongoplayerlib.BongoPlayerBuilder;
import com.bongobd.bongoplayerlib.BongoPlayerView;
import com.bongobd.bongoplayerlib.VideoPlayerEvents;

import butterknife.BindView;
import butterknife.ButterKnife;


public class VideoDetailsActivity extends AppCompatActivity
        implements VideoPlayerEvents.OnErrorListener,
        VideoPlayerEvents.OnBackButtonClickListener {

    private static final String TAG = "VideoDetailsActivity";
    public boolean onPauseState = false;


    @BindView(R.id.bongoPlayerViewVod)
    BongoPlayerView bongoPlayerView;
    @BindView(R.id.rlVodDetailsWrapper)
    RelativeLayout rlVodDetailsWrapper;

    Context mContext;
    String ratings;
    boolean favoriteStatus = false;
    boolean isConnectedToInternet;

    boolean isPlayerActive = false;
    // Tracker mTracker;
    String videoUrl = "";
    String videoTitle = "";
    String mLanguageStatus;
    private boolean isCommonContentSelectorLoaded = true;
    private ActionBar actionBar;
    private String bongoId;
    private String releaseText = "";

    private NetworkStateReceiver networkStateReceiver;

    private BongoPlayer bongoPlayer;
    private boolean isFullscreen;
    private Runnable mediaRunnable;
    private final Handler mediaHandler = new Handler();
    boolean isOnResumeState;
    private boolean isPlayerStopForNetworkError;
    //    private CastPresenterImpl castPresenter;
//    private CastItem castItem;
    private boolean wasPlaying;
    private long positionWasAt = 0;


    private boolean isEpisodeListOpen = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        ButterKnife.bind(this);

        playerViewSetup();
        bongoId = "bongoIdText";
        initialView();

        this.registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void runAnalyticsRunner() {
        mediaHandler.removeCallbacks(mediaRunnable);
        mediaRunnable = new Runnable() {
            @Override
            public void run() {
                if (bongoPlayer != null && bongoPlayer.isPlaying()) {
                    AnalyticsController.trackVideo(bongoPlayer);
                }
                mediaHandler.postDelayed(this, 10000);
            }
        };
        mediaHandler.postDelayed(mediaRunnable, 10000);

    }

    private void playerViewSetup() {
        if (bongoPlayerView != null) {
            bongoPlayerView.setFastForwardIncrementMs(10000);
            bongoPlayerView.setRewindIncrementMs(10000);
        }
        bongoPlayer = new BongoPlayerBuilder(this)
                .setBongoPlayerView(bongoPlayerView)
                .addOnErrorListener(this)
                .setAutoPlay(true)
                .setUserAgent("B Player")
                .setResizeMode(BongoPlayerView.RESIZE_MODE_FILL)
                .build();

        bongoPlayer.setOnBackButtonClickListener(this);
        bongoPlayer.setOnCompleteListener(new VideoPlayerEvents.OnCompleteListener() {
            @Override
            public void onComplete() {
                //mediaHandler.removeCallbacks(mediaRunnable);
                AnalyticsController.trackVideoComplete();
            }
        });
        wasPlaying = true;
        changePlayerScreenOrientation(getResources().getConfiguration());
    }

    public void initialView() {

        actionBar = getSupportActionBar();
        mContext = getApplicationContext();
        AnalyticsController.logLoadedContent(new Content());
        onPlayNewVideo("https://vod.bongobd.com/vod/vod/L/P/LPt93RyAWp3/LPt93RyAWp3.m3u8", null);

    }


    private void onPlayNewVideo(String vodUrl, String adsTag) {
        videoUrl = vodUrl;
        bongoPlayer.setTitle(videoTitle);
        if (adsTag != null) {
            bongoPlayer.load(vodUrl, adsTag);
        } else {
            bongoPlayer.load(vodUrl);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Exit fullscreen when the user pressed the Back button
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (bongoPlayerView.getFullScreen()) {
                bongoPlayer.setFullScreen(false);
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged() called with: newConfig = [" + newConfig + "]");
        changePlayerScreenOrientation(newConfig);
        super.onConfigurationChanged(newConfig);
    }

    private void changePlayerScreenOrientation(Configuration newConfig) {
        bongoPlayer.onConfigurationChanged(newConfig);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    protected void onDestroy() {
        if (mediaHandler != null) {
            mediaHandler.removeCallbacks(mediaRunnable);
        }
        // Let JW Player know that the app is being destroyed
        if (bongoPlayer != null) {
            try {
                isPlayerActive = false;
                bongoPlayer.onDestroy(); // Error is visible right here.
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

            super.onDestroy();

        }

        //EventBus.getDefault().unregister(this);

        this.unregisterReceiver(networkStateReceiver);
//        if (castPresenter != null) {
//            castPresenter.clear();
//        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
        //  rlChannelListContainer.setVisibility(View.INVISIBLE);
        //  btnShowTvList.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //  getMenuInflater().inflate(R.menu.menu_jwplayer, menu);
        // Register the MediaRouterButton on the JW Player SDK
        // castManager.addMediaRouterButton(menu, R.id.media_route_menu_item);
        return true;
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        AnalyticsController.logGlobalActivityScreen(this,"VideoDetails");
        isOnResumeState = true;
        isPlayerActive = true;
        if (!isCommonContentSelectorLoaded) {
            if (isConnectedToInternet && wasPlaying) {
                bongoPlayer.onResume();
            } else {
                bongoPlayer.onPause();
            }
        }
        super.onResume();
        invalidateOptionsMenu();
        onPauseState = false;
        runAnalyticsRunner();


    }


//    @Override
//    public void onContentPlay(String videoUrl, String adsTag) {
//        runAnalyticsRunner();
//        onPlayNewVideo(videoUrl,adsTag);
//
//        Log.d(TAG, "onContentPlay() called with: videoUrl = [" + videoUrl + "]");
//    }


    @Override
    public void onBackButtonClicked() {
        if (bongoPlayer.isFullScreen() == 1) {
            bongoPlayer.setFullScreen(false);
        } else {
            finish();
        }
    }


    public static void open(Context context, String bongoId) {
        Intent intent = new Intent(context, VideoDetailsActivity.class);
        context.startActivity(intent);
    }


    @Override
    public void onError(String s) {
        if (s.contains("No connection")) {
            isPlayerStopForNetworkError = true;
        }
        Log.d(TAG, "onError() called with: s = [" + s + "]");
    }


}
