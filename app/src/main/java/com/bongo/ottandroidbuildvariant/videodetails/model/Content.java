
package com.bongo.ottandroidbuildvariant.videodetails.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Content {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("subtitle")
    @Expose
    private String subtitle;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("publicationDate")
    @Expose
    private String publicationDate;
    @SerializedName("basedOn")
    @Expose
    private String basedOn;
    @SerializedName("concept")
    @Expose
    private String concept;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("youtubeId")
    @Expose
    private String youtubeId;
    @SerializedName("kalturaId")
    @Expose
    private String kalturaId;
    @SerializedName("elementalId")
    @Expose
    private String elementalId;
    @SerializedName("achivement")
    @Expose
    private String achivement;
    @SerializedName("specialCredits")
    @Expose
    private String specialCredits;


    @SerializedName("rating")
    @Expose
    private String rating;


    @Expose
    private String videoUrl;
    @SerializedName("releasedYear")
    @Expose
    private Integer releasedYear;
    @SerializedName("tvioViews")
    @Expose
    private Integer tvioViews;
    @SerializedName("bongoId")
    @Expose
    private String bongoId;
    @SerializedName("likeCount")
    @Expose
    private Integer likeCount;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return "test_title";
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Object getBasedOn() {
        return basedOn;
    }

    public void setBasedOn(String basedOn) {
        this.basedOn = basedOn;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getKalturaId() {
        return kalturaId;
    }

    public void setKalturaId(String kalturaId) {
        this.kalturaId = kalturaId;
    }

    public Object getElementalId() {
        return elementalId;
    }

    public void setElementalId(String elementalId) {
        this.elementalId = elementalId;
    }

    public String getAchivement() {
        return achivement;
    }

    public void setAchivement(String achivement) {
        this.achivement = achivement;
    }

    public String getSpecialCredits() {
        return specialCredits;
    }

    public void setSpecialCredits(String specialCredits) {
        this.specialCredits = specialCredits;
    }


    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }



    public String getVideoUrl() {
        return "https://vod.bongobd.com/vod/vod/L/P/LPt93RyAWp3/LPt93RyAWp3.m3u8";
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Integer getReleasedYear() {
        return releasedYear;
    }

    public void setReleasedYear(Integer releasedYear) {
        this.releasedYear = releasedYear;
    }

    public Integer getTvioViews() {
        return tvioViews;
    }

    public void setTvioViews(Integer tvioViews) {
        this.tvioViews = tvioViews;
    }

    public String getBongoId() {
        return "testBongoId";
    }

    public void setBongoId(String bongoId) {
        this.bongoId = bongoId;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }


}
