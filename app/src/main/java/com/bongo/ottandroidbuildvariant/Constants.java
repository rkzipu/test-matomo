package com.bongo.ottandroidbuildvariant;


/**
 * Created by hasanaits on 4/26/17.
 */

public  interface Constants  {

    public static String KEY_LOGGED_IN_USER_CHANNEL = "logged_in_user_channel";

    public static final String PAYMENT_GATEWAY_STRIPE = "STRIPE";
   // public static final String PAYMENT_GATEWAY_DIGI = "DIGI";
    public static final String PAYMENT_GATEWAY_TRIAL = "TRIAL";
    String BANGLALINK_PLATFORM_NAME ="bangla-link" ;
    String BANGLA_LINK_MSISDN_HOST = "http://bl-api.bongobd.com/";
    String APP_SUBSCRIPTION =  "appSubscription";
    String PAYMENT_GATEWAY_BANGLALINK ="banglalink" ;
    Integer BL_PIWIK_ID = 28;
    Integer BONGO_PIWIK_ID = 27 ;

    //   public static final String PAYMENT_GATEWAY_ETISALAT = "ETISALAT";
   // public static final String PAYMENT_GATEWAY_DU = "DU";



}
