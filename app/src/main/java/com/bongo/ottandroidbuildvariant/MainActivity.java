package com.bongo.ottandroidbuildvariant;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bongo.ottandroidbuildvariant.analytics.AnalyticsController;
import com.bongo.ottandroidbuildvariant.videodetails.view.VideoDetailsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, VideoDetailsActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        AnalyticsController.logGlobalActivityScreen(this,"MainActivity");
    }
}
