package com.bongo.ottandroidbuildvariant.analytics;

import com.bongo.ottandroidbuildvariant.BuildConfig;

import org.matomo.sdk.TrackerBuilder;

public class AnalyticsConfig {
    public static TrackerBuilder getTrackerBuilder() {
        return TrackerBuilder.createDefault("https://staging-stat.bongobd.com/matomo.php", 43);
    }
}
