package com.bongo.ottandroidbuildvariant.analytics;

import android.net.TrafficStats;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Zipu on 3/28/2018.
 */

public class UserBandwidthConsumption {
   public static int UID = android.os.Process.myUid();
   private long userTotalDataUsed;
   private long userDataUsed;
   private long userDataUsedBytes;
   private long mStartRX = 0;
   private long mStartTX = 0;
//    rxBytes += getUidRxBytes(UID);
//    txBytes += getUidTxBytes(UID);

    public UserBandwidthConsumption() {
        initCurrentUsedBytes();
    }

    /**
     * Read UID Rx Bytes
     *
     * @param// uid
     * @return rxBytes
     */


    private Long getUidRxBytes() {
        BufferedReader reader;
        Long rxBytes = 0L;
        try {
            reader = new BufferedReader(new FileReader("/proc/uid_stat/" + UserBandwidthConsumption.UID
                    + "/tcp_rcv"));
            rxBytes = Long.parseLong(reader.readLine());
            reader.close();
        }
        catch (FileNotFoundException e) {
            rxBytes = TrafficStats.getUidRxBytes( UserBandwidthConsumption.UID);
            //e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return rxBytes;
    }

    /**
     * Read UID Tx Bytes
     *
     * @param //uid
     * @return txBytes
     */
    private Long getUidTxBytes() {
        BufferedReader reader;
        Long txBytes = 0L;
        try {
            reader = new BufferedReader(new FileReader("/proc/uid_stat/" +  UserBandwidthConsumption.UID
                    + "/tcp_snd"));
            txBytes = Long.parseLong(reader.readLine());
            reader.close();
        }
        catch (FileNotFoundException e) {
            txBytes = TrafficStats.getUidTxBytes( UserBandwidthConsumption.UID);
            //e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return txBytes;
    }

    private void initCurrentUsedBytes(){
        mStartRX = getUidRxBytes();
        mStartTX = getUidTxBytes();
    }
    public void resetDataConsumtion(){
        initCurrentUsedBytes();
        userTotalDataUsed=0;
        userDataUsed=0;
        userDataUsedBytes=0;
    }

    public long getUserTotalDataUsed() {
        return userTotalDataUsed;
    }

    public void setUserTotalDataUsed(long userTotalDataUsed) {
        this.userTotalDataUsed = userTotalDataUsed;
    }

    public long getUserDataUsed() {
        return userDataUsed;
    }

    public void setUserDataUsed(long userDataUsed) {
        this.userDataUsed = userDataUsed;
    }

    public long getUserDataUsedBytes() {
        return userDataUsedBytes;
    }

    public void setUserDataUsedBytes(long userDataUsedBytes) {
        this.userDataUsedBytes = userDataUsedBytes;
    }

    public void calculateUsedBandwidth(){
         userDataUsedBytes=getUidRxBytes() - mStartRX;
         userDataUsed=getUidRxBytes() - mStartRX;
         userTotalDataUsed+=userDataUsed;
         initCurrentUsedBytes();
    }

    @Override
    public String toString() {
        return "UserBandwidthConsumption{" +
                "userTotalDataUsed=" + userTotalDataUsed +
                ", userDataUsed=" + userDataUsed +
                ", userDataUsedBytes=" + userDataUsedBytes +
                ", mStartRX=" + mStartRX +
                ", mStartTX=" + mStartTX +
                '}';
    }
}
