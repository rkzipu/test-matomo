package com.bongo.ottandroidbuildvariant.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.bongo.ottandroidbuildvariant.MainApplication;



import com.bongo.ottandroidbuildvariant.videodetails.model.Content;
import com.bongobd.bongoplayerlib.BongoPlayer;


import org.matomo.sdk.QueryParams;
import org.matomo.sdk.TrackMe;
import org.matomo.sdk.Tracker;
import org.matomo.sdk.extra.DimensionQueue;
import org.matomo.sdk.extra.DownloadTracker;
import org.matomo.sdk.extra.MatomoApplication;
import org.matomo.sdk.extra.TrackHelper;

import java.util.Random;
import java.util.UUID;



/**
 * Created by Hridoy on 25-Sep-17.
 */

public class AnalyticsController {
    public static boolean isInitializedMatomo = false;
    private static final String TAG = "AnalyticsController";
    public static final String KEY_PIWIK_USER_ID = "piwik_user_id_key";
    public static final String USER_ID_NOT_SET = "default_user_id_piwik";
    private static TrackMeBuilder trackMeBuilder;
    private static UserBandwidthConsumption userBandwidthConsumption;
    private static long contentImprTime;
    private static long ttp;
    private static long maSt;
    private static DimensionQueue mDimensionQueue;


    private static long lastActionTime;


    public static UserBandwidthConsumption getUserBandwidthConsumption() {
        if (userBandwidthConsumption == null) {
            userBandwidthConsumption = new UserBandwidthConsumption();
            return userBandwidthConsumption;
        }

        return userBandwidthConsumption;
    }

    private static void logCurrentScreen(Context context, String screen, String title) {
        TrackMe trackMe = new TrackMe();
        trackMe.set(QueryParams.URL_PATH, "/" + screen);
        trackMe.set(QueryParams.ACTION_NAME, title);
        trackEvent(trackMe);

        Log.d(TAG, "logCurrentScreen() called with: context = [" + context + "], screen = [" + screen + "], title = [" + title + "]");
    }

    /*Piwik*/
    private static Tracker getTracker() {
        return MainApplication.getInstance().getTracker();
    }

    private static void initRequirdVar() {
        maSt = 0;
        contentImprTime = System.currentTimeMillis();
        AnalyticsController.trackMeBuilder = new TrackMeBuilder();
        getUserBandwidthConsumption().resetDataConsumtion();
    }

    /*Piwik*/
    public static void logLoadedContent(Content content) {
        initRequirdVar();
        try {
            if (content != null) {
                resetTtp();
                if (trackMeBuilder != null) {
                    trackMeBuilder.setMa_id(getRandomString(content.getBongoId()));
                    trackMeBuilder.setMa_ti("VIDEO:" + content.getTitle());
                    trackMeBuilder.setMa_pn("Android: BPlayer");
                    trackMeBuilder.setMa_mt("video");
                    trackMeBuilder.setMa_re(content.getVideoUrl());
                    trackEvent(trackMeBuilder);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }




    /*Piwik*/
    public static void trackVideo(BongoPlayer bongoPlayer) {

        try {
            if (trackMeBuilder != null) {
                if (bongoPlayer != null) {
                    maSt += 10;
                    trackMeBuilder.setMa_fs(bongoPlayer.isFullScreen());
                    trackMeBuilder.setMa_w(bongoPlayer.getVideoWidth());
                    trackMeBuilder.setMa_h(bongoPlayer.getVideoHeight());
                    trackMeBuilder.setMa_st(maSt);
                    trackMeBuilder.setMa_ttp(getTtp());
                    trackMeBuilder.setMa_ps(bongoPlayer.getCurrentPosition() / 1000);
                    trackMeBuilder.setMa_le(bongoPlayer.getDuration() / 1000);
                } else {
                    trackMeBuilder.setMa_st(0);
                }
                trackBandWidth(trackMeBuilder);
               // trackEvent(trackMeBuilder);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private static void extendSessionTimeout(long duration) {
        getTracker().setSessionTimeout((int) (duration+(5*60*1000)));
    }


    private static void trackBandWidth(TrackMe trackMe) {
        try {
            if (userBandwidthConsumption != null) {
                userBandwidthConsumption.calculateUsedBandwidth();
                Log.d(TAG, "setUserBandwidthConsumption() called with: userBandwidthConsumption = [" + userBandwidthConsumption.toString() + "]");
             //   TrackMe trackMe = new TrackMe();
                trackMe.set("idgoal", Integer.toString(1));
                trackMe.set("data_used", Long.toString(userBandwidthConsumption.getUserDataUsed()));
                trackMe.set("bw_bytes", Long.toString(userBandwidthConsumption.getUserDataUsed()));
                trackMe.set("total_data_used", Long.toString(userBandwidthConsumption.getUserTotalDataUsed()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        trackEvent(trackMe);

    }

    public static void trackVideoComplete() {
        try {
            if (trackMeBuilder != null) {
                trackMeBuilder.set("e_a", "finish");
                Log.d(TAG, "trackVideoComplete() called");
                trackEvent(trackMeBuilder);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void resetTtp() {
        contentImprTime = System.currentTimeMillis();
        ttp = 0;
    }

    private static long getTtp() {
        if (ttp > 0) return ttp;
        ttp = (System.currentTimeMillis() - contentImprTime) / 1000;
        return ttp;
    }

    private static void trackEvent(TrackMe trackMe){
        if (trackMe == null) {
            return;
        }

        long currentTimeMillis = System.currentTimeMillis();
        if((currentTimeMillis-lastActionTime)>=getTracker().getSessionTimeout()){
            extendSessionTimeout(getTracker().getSessionTimeout()+(30*60*1000));
        }
        lastActionTime = currentTimeMillis;

        if(!isInitializedMatomo){
            onInitTracker(MainApplication.getInstance(),null);
            initMatomoUserId();
        }
        Log.d(TAG, "current session timeout called with: trackMe = [" + getTracker().getSessionTimeout()/1000 + "]");
        getTracker().track(trackMe);
    }
    private static String getRandomString(String maId) {
        final int sizeOfRandomString = 15;
        final Random random = new Random();
        String uuidString = maId + UUID.randomUUID().toString().replaceAll("-", "");
        final StringBuilder sb = new StringBuilder(sizeOfRandomString);
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(uuidString.charAt(random.nextInt(uuidString.length())));
        Log.d(TAG, "getRandomString() called with: maId = [" + sb.toString() + "]");
        return sb.toString();
    }






    public static void initMatomoUserId() {
        getTracker().setUserId("88018178982909[18]");
        getTracker().getDefaultTrackMe().set("cip", "192.168.1.18");
    }

    public static void onInitTracker(MatomoApplication matomoApplication, String userid) {
        isInitializedMatomo = true;


        Tracker tracker = matomoApplication.getTracker();
       tracker.setDispatchInterval(0);
        // When working on an app we don't want to skew tracking results.
        // getMatomo().setDryRun(BuildConfig.DEBUG);

        // If you want to set a specific userID other than the random UUID token, do it NOW to ensure all future actions use that token.
        // Changing it later will track new events as belonging to a different user.
        // String userEmail = ....preferences....getString
        if (userid != null) {
            tracker.setUserId(userid);
        }

        // Track this app install, this will only trigger once per app version.
        // i.e. "http://org.matomo.demo:1/185DECB5CFE28FDB2F45887022D668B4"
        TrackHelper.track().download().identifier(new DownloadTracker.Extra.ApkChecksum(matomoApplication)).with(tracker);
        // Alternative:
        // i.e. "http://org.matomo.demo:1/com.android.vending"
        // getTracker().download();

        mDimensionQueue = new DimensionQueue(getTracker());

        // This will be send the next time something is tracked.
        mDimensionQueue.add(0, "test");

        tracker.addTrackingCallback(new Tracker.Callback() {
            @Nullable
            @Override
            public TrackMe onTrack(TrackMe trackMe) {
                return trackMe;
            }
        });
    }




    public static void logGlobalActivityScreen(Context context, String title) {
        try {
            String simpleName = context.getClass().getSimpleName();
            if(title!=null&&!title.isEmpty()){
                logCurrentScreen(context,simpleName,title);
            }else {
                logCurrentScreen(context,simpleName,getScreenTitle(simpleName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void logGlobalFragment(FragmentActivity activity, String fragmentName, String title) {
        try {
            if(title!=null&&!title.isEmpty()){
                logCurrentScreen(activity,fragmentName,title);
            }else {
                logCurrentScreen(activity,fragmentName,getScreenTitle(fragmentName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static String getScreenTitle(String simpleName) {
        simpleName = simpleName.replace("Activity","");
        simpleName = simpleName.replace("Fragment","");
        Log.d(TAG, "getScreenTitle() called with: simpleName = [" + simpleName + "]");
        return simpleName;
    }

}
