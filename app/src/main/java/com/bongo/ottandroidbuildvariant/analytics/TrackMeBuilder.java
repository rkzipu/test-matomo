package com.bongo.ottandroidbuildvariant.analytics;

import androidx.annotation.NonNull;

import org.matomo.sdk.TrackMe;


/**
 * Created by Zipu on 2/25/2018.
 */

public class TrackMeBuilder extends TrackMe {
    private static final String TAG = "TrackMeBuilder";
//    
//        private String ma_id;
//        private String ma_ti;
//        private String ma_pn;
//        private String ma_mt;
//        private String ma_re;
//        private long ma_st;
//        private long ma_ps;
//        private long ma_le;
//        private long ma_ttp;
//        private int ma_w;
//        private int ma_h;
//        private int ma_fs;


    public TrackMeBuilder() {

    }

    public TrackMe setMa_id(String ma_id) {
        return set("ma_id", ma_id);
    }

    public TrackMe setMa_ti(String ma_ti) {
        return set("ma_ti", ma_ti);
    }

    public TrackMe setMa_pn(String ma_pn) {
        return set("ma_pn", ma_pn);
    }

    public TrackMe setMa_mt(String ma_mt) {
        return set("ma_mt", ma_mt);
    }

    public TrackMe setMa_re(String ma_re) {
        return set("ma_re", ma_re);
    }

    public TrackMe setMa_st(long ma_st) {
        return set("ma_st", Long.toString(ma_st));
    }

    public TrackMe setMa_ps(long ma_ps) {
        return set("ma_ps", Long.toString(ma_ps));
    }

    public TrackMe setMa_le(long ma_le) {
        return set("ma_le", Long.toString(ma_le));
    }

    public TrackMe setMa_ttp(long ma_ttp) {
        return set("ma_ttp", Long.toString(ma_ttp));
    }

    public TrackMe setMa_w(int ma_w) {
        return set("ma_w", Integer.toString(ma_w));
    }

    public TrackMe setMa_h(int ma_h) {
        return set("ma_h", Integer.toString(ma_h));
    }


    public TrackMe setMa_fs(int ma_fs) {
        return this.set("ma_fs", Integer.toString(ma_fs));
    }


    @Override
    public synchronized TrackMe set(@NonNull String key, String value) {
        return super.set(key, value);
    }

    public TrackMe build() {
        return this;
    }
}
