package com.bongo.ottandroidbuildvariant;

import android.content.Context;
import android.location.Location;
import android.os.StrictMode;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.bongo.ottandroidbuildvariant.analytics.AnalyticsConfig;
import com.bongo.ottandroidbuildvariant.utils.AssetController;


import org.matomo.sdk.Tracker;
import org.matomo.sdk.TrackerBuilder;
import org.matomo.sdk.extra.MatomoApplication;




public class MainApplication extends MatomoApplication {
    private static final String TAG = "MainApplication";

   // private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    private static MainApplication mInstance;
    //        TODO set countryCode null when geo-blocking required

    public static Location location=null;



    public static MainApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        AssetController.getInstance(getApplicationContext());
//        CastManager.initialize(this);

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
        //onInitTracker();

    }


    @Override
    public TrackerBuilder onCreateTrackerConfig() {
       return AnalyticsConfig.getTrackerBuilder();
    }

    @Override
    public void onTrimMemory(final int level) {
        Log.d(TAG, "onTrimMemory " + level);

        super.onTrimMemory(level);
    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }




}
