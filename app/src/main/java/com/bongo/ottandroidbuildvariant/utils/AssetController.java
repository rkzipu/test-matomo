package com.bongo.ottandroidbuildvariant.utils;

import android.content.Context;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;

import com.bongo.ottandroidbuildvariant.R;

/**
 * Created by hasanaits on 4/26/17.
 */

public class AssetController {

    public static AssetController assetController;


    Typeface typeFaceBold;
    Typeface typeFaceMedium;
    Typeface typeFaceRegular;


    public static AssetController getInstance(Context context) {
        if(assetController==null) {
            assetController= new AssetController();
            assetController.initSettings(context);
            return assetController;
        }
        return assetController;

    }

    public void initSettings(Context context) {
        // Get value of first launch
        // Get value of login
        // Get value of language

        typeFaceRegular = ResourcesCompat.getFont(context, R.font.default_font_regular);
        typeFaceMedium = ResourcesCompat.getFont(context, R.font.default_font_medium);
        typeFaceBold = ResourcesCompat.getFont(context, R.font.default_font_bold);


        //isUserLoggedIn = SharedPreferencesUtils.getInstance().getBooleanValue(Constants.KEY_LOGIN_STATUS, false);

    }

    public Typeface getTypeFaceBold() {
        return typeFaceBold;
    }

    public Typeface getTypeFaceMedium() {
        return typeFaceMedium;
    }

    public Typeface getTypeFaceRegular() {
        return typeFaceRegular;
    }


}
