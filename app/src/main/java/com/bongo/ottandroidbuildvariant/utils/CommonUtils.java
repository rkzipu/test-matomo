package com.bongo.ottandroidbuildvariant.utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.bongo.ottandroidbuildvariant.BuildConfig;
import com.bongo.ottandroidbuildvariant.R;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }
    public static int getDeviceHeight(Context context) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    /**
     * Convert DP to Pixel
     *
     * @param dp
     * @param ctx
     * @return
     */
    public static int dip2px(float dp, Context ctx) {
        return (int) (dp * ctx.getResources().getDisplayMetrics().density + 0.5);
    }

    /**
     * Convert Pixel to DP
     *
     * @param px
     * @param ctx
     * @return
     */
    public static float px2dip(int px, Context ctx) {
        return (float) px / ctx.getResources().getDisplayMetrics().density;
    }

    public static int getDeviceWidth(Context context) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getDeviceHeightByRatio(Context context, double ratioValue) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        float pxHeight = (float)(ratioValue * (double)((float)displayMetrics.widthPixels));
        return (int)pxHeight;
    }
    public static ProgressDialog initProgressDialog(Context context, String msg) {
        if (TextUtils.isEmpty(msg)) {
            msg = "Loading...";
        }

        ProgressDialog progressDialog = null;
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(msg);
            progressDialog.setCancelable(false);
        } catch (Exception e) {
            Log.e("ProgressDialog", "Can Not Initiate Progress Dialog" + e + "");
        }


        return progressDialog;
    }

    public static ProgressDialog getProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
//        ColorDrawable colorDrawable = new ColorDrawable(ContextCompat.getColor(context, R.color.progress_dialog_background_color));
//        if (progressDialog.getWindow() != null) {
//            progressDialog.getWindow().setBackgroundDrawable(colorDrawable);
//        }
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName)
            throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }


    public static String getAccessCode(String countryCode) {
        if (countryCode != null) {
            try {
                String base64AccessCode = Base64.encodeToString(countryCode.getBytes("UTF-8"), Base64.DEFAULT);
                return URLEncoder.encode(base64AccessCode.trim(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static File reduceFileSize(File file) {
        File file1 = file;

        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();


            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            //Log.e("error",e.getMessage());
            return file1;
        }
    }

    public static String getRandomString() {
        final int sizeOfRandomString = 16;
        final Random random = new Random();
        String uuidString = UUID.randomUUID().toString().replaceAll("-", "");
        final StringBuilder sb = new StringBuilder(sizeOfRandomString);
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(uuidString.charAt(random.nextInt(uuidString.length())));
        return sb.toString();
    }
    public static void primeTagVisible(boolean isPremium, View view){
        if (view != null) {
            view.setVisibility(isPremium? View.VISIBLE: View.GONE);
        }
        Log.d(TAG, "primeTagVisible() called with: isPremium = [" + isPremium + "], view = [" + view + "]");
    }

    public static boolean isBanglaLink(String msisdn) {
        if(msisdn == null)return false;
        String substring = msisdn.substring(0, 5);
        Log.d(TAG, "isBanglaLink() called with: msisdn = [" + msisdn + "]");
        return "88019".equalsIgnoreCase(substring)||"88014".equalsIgnoreCase(substring);
    }

    public static boolean isRobiNumber(String msisdn) {
        if(msisdn == null)return false;
        String substring = msisdn.substring(0, 5);
        return "88018".equalsIgnoreCase(substring);
    }

    public static boolean isAirtelNumber(String msisdn) {
        if(msisdn == null)return false;
        String substring = msisdn.substring(0, 5);
        return "88016".equalsIgnoreCase(substring);
    }

    private static boolean isGpNumber(String msisdn) {
        if(msisdn == null)return false;
        String substring = msisdn.substring(0, 5);
        Log.d(TAG, "isGpNumber() called with: msisdn = [" + msisdn + "]");
        return "88017".equalsIgnoreCase(substring)||"88013".equalsIgnoreCase(substring);
    }
    public static boolean isTeletalkNumber(String msisdn) {
        if(msisdn == null)return false;
        String substring = msisdn.substring(0, 5);
        return "88015".equalsIgnoreCase(substring);
    }
    public static boolean isFlavorRobiRedBox() {
        return BuildConfig.FLAVOR.equalsIgnoreCase("robiredbox");
    }

    public static boolean isFlavorBongoBd() {
        return BuildConfig.FLAVOR.equalsIgnoreCase("bongoBd");
    }

    public static boolean isFlavorAirtelRedBox() {
        return BuildConfig.FLAVOR.equalsIgnoreCase("airtelredbox");
    }

    public static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }



}
